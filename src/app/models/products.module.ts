import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Observable } from 'rxjs/Observable';

export class Item {
  $key: string;
  name: string;
  description: string;
  price: number;
  title: string;
}

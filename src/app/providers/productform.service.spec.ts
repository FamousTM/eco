import { TestBed, inject } from '@angular/core/testing';

import { ProductformService } from './productform.service';

describe('ProductformService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ProductformService]
    });
  });

  it('should be created', inject([ProductformService], (service: ProductformService) => {
    expect(service).toBeTruthy();
  }));
});

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomepageComponent } from './pages/cms/homepage/homepage.component';
import { CategoriesComponent } from './pages/categories/categories.component';
import { ProductsComponent } from './pages/categories/products/products.component';
// import { FormComponent } from './pages/categories/products/form/form.component';
import { LoginComponent } from './pages/login/login.component';
import { PnfComponent } from './pages/pnf/pnf.component';
import { UsersComponent } from './users/users.component';
import { UserProfileComponent } from './pages/user-profile/user-profile.component';
import { AuthGuard } from './core/auth.guard';

import { AuthService } from './core/auth.service';

const routes: Routes = [
  { path: '', component: HomepageComponent },
  { path: 'categories', component: CategoriesComponent },
  // { path: 'user-profile', component: UserProfileComponent,  canActivate: [AuthGuard] },
  { path: 'login', component: LoginComponent },
  { path: 'categories', component: CategoriesComponent },
  // { path: 'products/form', component: FormComponent },
  { path: 'users', component: UsersComponent },
  { path: 'products/:name', component: ProductsComponent },
  { path: '**', component: PnfComponent }
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

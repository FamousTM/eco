import { Component, OnInit } from '@angular/core';

import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument, fromCollectionRef } from 'angularfire2/firestore';

import { ActivatedRoute, Params, Router } from '@angular/router';
import { Key } from 'selenium-webdriver';

import { Subject } from 'rxjs/Subject';

import { FirestoreService } from '../../../providers/firestore.service';

export interface Items { id: string; title: string; params: string; }

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {

  private routeSubscribed: any;
  private items: any;
  private id: any;


  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private db: AngularFirestore,
    private firestoreservice: FirestoreService
  ) {

      this.items = this.activatedRoute.params
      .switchMap(params =>
       db.collection('items/') // this points to the collection name
       .doc('tops') // this points to the document name
       .valueChanges());
       console.log(this.db);
       // Add a new document with a generated id.
      }

     ngOnInit() { // the router below gets the activated route param
      this.routeSubscribed = this.activatedRoute.params.subscribe(params => {
        this.activatedRoute.params.subscribe(res => {
          console.log(params);
        });
        console.log('these are the params under products =>', params);
      });

    }

  }

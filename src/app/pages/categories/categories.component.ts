import { Component, OnInit } from '@angular/core';

import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
import { Observable } from 'rxjs/Observable';

import { ActivatedRoute, Params, Router } from '@angular/router';

import { FirestoreService } from '../../providers/firestore.service';
import { Item } from '../../models/products.module';


@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss']
})
export class CategoriesComponent implements OnInit {

  ref: AngularFirestoreCollection<Item>; // famous this is needed to reference Items in the collection
  items: Observable<Item[]>;

  user: any;
  private routeSubscribed: any;

  constructor(
    private afs: AngularFirestore,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    public firestoreservice: FirestoreService,
    private afAuth: AngularFireAuth,
  ) {
    this.user = afAuth.authState;
    // this.items = db.collection('items').valueChanges();
  }
  ngOnInit() {


    this.items = this.firestoreservice.colWithIds$('items');

    console.log('test');

    this.routeSubscribed = this.activatedRoute.params.subscribe(params => {
      this.activatedRoute.params.subscribe(res => {
        console.log(res.id);
      });
      console.log('these are the params under categories =>', params);

    });

  }
}
